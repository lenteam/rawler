# -*- coding: utf-8 -*-
import re
import gzip
import requests
import pandas as pd
from bs4 import BeautifulSoup
from connector import Connector
from connector import IntegrityError
from datetime import datetime, date, timedelta
import string
from itertools import chain




# 1. Найти новые сайты для обхода.
# Инициализировать начало обхода:
# Просмотреть таблицу Sites.
# Найти строки в таблице Sites, которым не соответствует НИ ОДНОЙ строки в таблице Pages. 
# Для этого сайта добавить в таблицу Pages строку с 
# URL равным http://<имя_сайта>/robots.txt и LastScanDate равным null.


def initialization(cnx):
    # Извлекаем все сайты у которых нет страниц    
    new_sites = pd.read_sql_query(
        """
        SELECT ID, Name
        FROM Sites
        WHERE ID NOT IN (
            SELECT DISTINCT SiteID
            FROM Pages
            )
        ;
        """
        , cnx.conn
    )

    if not new_sites.empty:
        # Для сайтов без страниц создаем ссылки robots.txt
        # и заносим их в БД
        cur_date = datetime.strftime(date.today(), '%Y-%m-%d')   
        c = cnx.conn.cursor()
        c.executemany(
            """
                INSERT INTO Pages (SiteID, Url, FoundDateTime) 
                VALUES (%s, %s, %s)
                ;
            """
            , [(str(values[0])
                , "{0}/robots.txt".format(values[1].rstrip("/"))
                , cur_date) for key, values in new_sites.iterrows()])
        
        cnx.conn.commit()
        c.close()

# 2. Обход ссылок, которых по которым раньше не производился обход
# Найти в таблице Pages ссылку, у которой LastScanDate равен null.
# Скачать HTML по данной ссылке
# В зависимости от того, что это была за ссылка, выполняем следующие действия:
# i. Если ссылка ведет на robots.txt, находим в HTML-е ссылку на sitemap, и добавляем ее в таблицу Pages
# ii. Если ссылка ведет на sitemap, находим в HTML-е все ссылки с сайта, добавляем их в Pages.
# iii. Если ссылка ведет на страницу сайта, найти количество вхождений ключевых слов для каждой личности, и сохранить 
# результаты в таблице PersonPageRank.

def get_new_data(cnx):
    
    def get_robots_txt(cnx):
        # Извлекаем все не просмотренные ссылки robots.txt
        robots_txt = pd.read_sql_query(
            """
                SELECT ID, Url, SiteID
                FROM Pages
                WHERE LastScanDate IS NULL
                AND Url LIKE '%/robots.txt'
                ;   
            """
        , cnx.conn
        )

        if not robots_txt.empty:
            # Текущая дата
            cur_date = datetime.strftime(date.today(), '%Y-%m-%d')
            c = cnx.conn.cursor()
            
            for key, values in robots_txt.iterrows():
                ID, Url, SiteID = values
                
                # Заполняем поле LastScanDate у ссылок robots.txt
                c.execute(
                    """
                        UPDATE Pages 
                        SET LastScanDate = %s
                        WHERE ID = %s
                        ;
                    """, (cur_date, ID)
                    )
                cnx.conn.commit()
                
                # Делаем запрос и записываем, если есть ссылки на Sitemaps в БД
                r = requests.get(Url)
                if r.status_code == 200:
                    soup = BeautifulSoup(r.text, "html.parser")
                    smps = re.findall(r'sitemap:\s+(https?://.*?)[\s|$]',
                                   soup.text, re.IGNORECASE)
                    if len(smps) > 0:
                        c.executemany(
                            """
                                INSERT INTO Pages (Url, SiteID, FoundDateTime)
                                VALUES (%s, %s, %s)
                                ;
                            """, [(str(Url), str(SiteID), cur_date) for Url in smps]
                            )
                        cnx.conn.commit()
            c.close()
    
    def get_sitemaps(cnx):
            # Извлекаем все не просмотренные ссылки на .xml и .xml.gz файлы
            xml_links = pd.read_sql_query(
                """
                    SELECT ID, Url, SiteID
                    FROM Pages
                    WHERE LastScanDate IS NULL
                        AND (Url LIKE '%.xml' OR Url LIKE '%.xml.gz')
                    ;   
                """
            , cnx.conn
            )

            if not xml_links.empty:
                c = cnx.conn.cursor()
                cur_date = datetime.strftime(date.today(), '%Y-%m-%d')

                # Для каждой ссылки из БД создаем запрос, и делаем отметку о посещении
                for key, values in xml_links.iterrows():
                    ID, Url, SiteID = values

                    # Заполняем поле LastScanDate для ссылок .xml и .xml.gz
                    c.execute(
                        """
                            UPDATE Pages 
                            SET LastScanDate = %s
                            WHERE ID = %s
                            ;
                        """, (cur_date, str(ID))
                        )
                    cnx.conn.commit()

                    # Делаем запрос по ссылке .xml / .xml.gz
                    r = requests.get(Url)
                    if r.status_code == 200:
                        # Если требуется распаковка - распаковываем
                        if Url.endswith('xml.gz'):
                            try:
                                content = gzip.decompress(r.content)
                            except:
                                content = ''
                        else:
                            content = r.content

                        soup = BeautifulSoup(content, "html.parser")

                        # Находим ссылки на страницы сайта либо ссылки на другие sitemap
                        urls = [x.text for x in soup.select('url loc')]
                        sitemaps = [x.text for x in soup.select('sitemap loc')]

                        # Записываем все найденные ссылки в БД
                        if len(urls) + len(sitemaps) > 0:
                            for page in chain(urls, sitemaps):
                                try:
                                    c.execute(
                                        """
                                            INSERT INTO Pages (Url, SiteID, FoundDateTime)
                                            VALUES (%s, %s, %s)
                                            ;
                                        """, (str(page), str(SiteID), cur_date)
                                    )
                                    cnx.conn.commit()
                                except IntegrityError:
                                    continue
                c.close()
            
    def colmen(cnx, PageID, Url, keywords):    
        result = pd.DataFrame()
        r = requests.get(Url)
        if r.status_code == 200:
            soup = BeautifulSoup(r.text, "html.parser")
            # Выбираем на страничке текст во всех параграфах и заголовках h1
            # Объединяем его в единый текст
            # Рзабиваем на отдельные слова
            # каждое слово очищаем от знаков препинания

            words = list(map(lambda x: x.strip(string.punctuation),
                    " ".join(chain([x.text for x in soup.select("p")]
                                   , [x.text for x in soup.select("h1")])).split() ))

            # Считаем сколько раз встретиловсь каждое ключевое слово
            keywords["Rank"] = keywords["Name"].map(lambda x: words.count(x))

            # Делаем группировку по PersonID с суммированием по Rank,
            # если ключевое слово встретилось хотя бы один раз
            result = keywords[keywords["Rank"] > 0].groupby(["PersonID"]).sum()
            # Если результат есть, заносим его в БД
            if len(result) > 0:
                c = cnx.conn.cursor()
                for PersonID, values in result.iterrows():
                    to_load = (str(PersonID), str(PageID), str(values[0]))
                    c.execute(
                        """
                                INSERT INTO PersonPageRank (PersonID, PageID, Rank) 
                                   VALUES (%s, %s, %s)
                                ;
                                """, to_load)
                    cnx.conn.commit()
    
    def get_pages(cnx):
        # Выбираем не посещенные странички
        pages_links = pd.read_sql_query(
            """
                SELECT ID, Url, SiteID
                FROM Pages
                WHERE LastScanDate IS NULL
                    AND Url NOT LIKE '%.xml'
                    AND Url NOT LIKE '%.xml.gz'
                    AND Url NOT LIKE '%/robots.txt'
                ;   
            """
            , cnx.conn
            )

        # Выбираем все ключевые слова
        keywords = pd.read_sql_query(
            """
                SELECT Name, PersonID
                FROM Keywords
                ;
            """
            , cnx.conn
            )

        if not pages_links.empty and not keywords.empty:
            c = cnx.conn.cursor()
            cur_date = datetime.strftime(date.today(), '%Y-%m-%d')

            # Для каждой ссылки из БД создаем запрос, и делаем отметку о посещении
            for key, values in pages_links.iterrows():
                ID, Url, SiteID = values

                # Заполняем поле LastScanDate
                c.execute(
                    """
                        UPDATE Pages 
                        SET LastScanDate = %s
                        WHERE ID = %s
                        ;
                    """, (cur_date, ID)
                    )
                cnx.conn.commit()

                # Считаем частоту по каждому ключевому слову и заносим результат в БД
                colmen(cnx, ID, Url, keywords)
    
    get_robots_txt(cnx)
    get_sitemaps(cnx)
    get_pages(cnx)

#3. Повторных обход по ссылкам, которые ранее обходил краулер
#Если в таблице Pages нет ссылок, у которых LastScanDate равен null, краулер делает повторных обход по тем ссылка, для которых #LastScanDate имеет наименьшее значение.
#Повторный обход осуществляем только по тем ссылкам, которые ведут на sitemap. 
#Повторный обход по ссылкам целесообразно делать не чаще чем раз в день.
def update_data(cnx):
    c = cnx.conn.cursor()
    cur_date = datetime.strftime(date.today(), '%Y-%m-%d')
    
    xml_links_to_update = pd.read_sql_query(
        """
            SELECT ID, Url, SiteID
            FROM Pages
            WHERE LastScanDate < %(cur_date)s
                AND (Url LIKE '%.xml'
                OR Url LIKE '%.xml.gz')
            ;   
        """
        , con=cnx.conn
        , params={'cur_date': cur_date}
    )
    
    if not xml_links_to_update.empty:
        for key, values in xml_links_to_update.iterrows():
            ID, Url, SiteID = values
            
            # Заполняем поле LastScanDate
            c.execute(
                """
                    UPDATE Pages 
                    SET LastScanDate = %s
                    WHERE ID = %s
                    ;
                """, (cur_date, ID)
                )
            cnx.conn.commit()

            # Делаем запрос по ссылке .xml / .xml.gz
            r = requests.get(Url)
            if r.status_code == 200:
                # Если требуется распаковка - распаковываем
                if Url.endswith('xml.gz'):
                    try:
                        content = gzip.decompress(r.content)
                    except:
                        content = ''
                else:
                    content = r.content

                soup = BeautifulSoup(content, "html.parser")

                # Находим ссылки на страницы сайта либо ссылки на другие sitemap
                urls = [x.text for x in soup.select('url loc')]
                sitemaps = [x.text for x in soup.select('sitemap loc')]

                # Записываем все найденные ссылки в БД
                if len(urls) + len(sitemaps) > 0:
                    for page in chain(urls, sitemaps):
                        try:
                            c.execute(
                                """
                                    INSERT INTO Pages (Url, SiteID, FoundDateTime)
                                    VALUES (%s, %s, %s)
                                    ;
                                """, (str(page), str(SiteID), cur_date)
                            )
                            cnx.conn.commit()
                        except IntegrityError:
                            continue
    c.close()

def main():
    while True:
        cnx = Connector()
        initialization(cnx)
        get_new_data(cnx)
        update_data(cnx)
        cnx.conn.close()

if __name__ == '__main__':
    main()