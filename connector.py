from mysql.connector import connection
from mysql.connector import IntegrityError

class Connector():
    conn_data = {
          "user" : "colmen"
        , "password" : ""
        , "host" : "localhost"
        , "database" : "colmen"
        }

    def __init__(self):
        self.conn = connection.MySQLConnection(**self.conn_data)