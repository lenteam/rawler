USE colmen;

INSERT INTO Persons (Name)
VALUES ('Путин');
INSERT INTO Persons (Name)
VALUES ('Медведев');
INSERT INTO Persons (Name)
VALUES ('Навальный');

INSERT INTO Sites (Name)
VALUES ('https://lenta.ru');


INSERT INTO Keywords (Name, PersonID) 
VALUES ('Путина', 1);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Путину', 1);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Путиным', 1);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Путине', 1);


INSERT INTO Keywords (Name, PersonID) 
VALUES ('Медведева', 2);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Медведеву', 2);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Медведевым', 2);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Медеведеве', 2);


INSERT INTO Keywords (Name, PersonID) 
VALUES ('Навального', 3);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Навальному', 3);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Навальным', 3);
INSERT INTO Keywords (Name, PersonID) 
VALUES ('Навальном', 3);

